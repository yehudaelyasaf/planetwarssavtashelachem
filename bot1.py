import random

changesList = []
def resetChangesList(pw):
    global changesList
    changesList = []
    changesList.append(0) #zero index
    
    for planet in pw.planets():
        changesList.append(0)

attackersList = []
def resetAttackersList():
    global attackersList
    attackersList = []

def updateChangesList(src, dst, num_ships):
    if src == dst:
        raise Exception('src == dst! beimashelachem!')
        
    changesList[src.planet_id()] -= num_ships
    changesList[dst.planet_id()] += num_ships

def sortFleets(e):
    return e.turns_remaining()

def defend(pw):
    if len(pw.enemy_fleets()) == 0:
        return False
    enemy_fleets = sorted(pw.enemy_fleets(), key=sortFleets)
    attacked_planets = []

    for fleet in enemy_fleets:
        attacked_planets.append(fleet.destination_planet())

    for fleet in enemy_fleets:
        if fleet.num_ships() == 0:
            continue
        dst = pw.get_planet(fleet.destination_planet())
        
        planets = []
        for planet in pw.my_planets():
            if planet.planet_id() in attacked_planets:
                continue
            i = 0
            for p2 in planets:
                if calculate_attack_grade(planet, dst, pw) < calculate_attack_grade(p2, dst, pw):
                    i += 1
            planets.insert(i, planet)

        fleet_num = fleet.num_ships()
        for i in range(len(planets)):
            if fleet_num == 0:
                return True
            if planets[i] == dst:
                pw.debug(f"{planets[i].planet_id()}")
                continue
            if planets[i].num_ships() == 0:
                continue
            if fleet_num > 10:
                if planets[i].num_ships() <= fleet_num/2:
                    continue
                pw.issue_order(planets[i], dst, fleet_num/2)
                updateChangesList(planets[i], dst, fleet_num/2)
                fleet_num /= 2
            else:
                if planets[i].num_ships() <= fleet_num:
                    continue
                pw.issue_order(planets[i], dst, fleet_num)
                updateChangesList(planets[i], dst, fleet_num)
                fleet_num = 0
    return True


def calculate_attack_grade(src, dst, pw):
    distance = pw.distance(src, dst)
    growthRate = dst.growth_rate()
    numShips = dst.num_ships() + changesList[dst.planet_id()]

    try:
        #distance**-1 * growthRate * numShips**-1
        attackGrade = float(100/distance) * growthRate * (100/(numShips))
    except ZeroDivisionError:
        attackGrade = 0

    return attackGrade

def attack(pw):
    myPlanets =  pw.my_planets()
    notMyPlanets = pw.not_my_planets()
    
    #if there are neutral planets, attack only them
    if len(pw.neutral_planets()) > 0:
        notMyPlanets = pw.neutral_planets()

    bestGrade = 0
    bestSrc = None
    bestDst = None
    global attackersList

    for myPlanet in myPlanets:
        if myPlanet.planet_id() in attackersList:
            #this planet has already attacked this turn
            continue

        for notMyPlanet in notMyPlanets:
            grade = calculate_attack_grade(myPlanet, notMyPlanet, pw)
            if grade > bestGrade:
                bestGrade = grade
                bestSrc = myPlanet
                bestDst = notMyPlanet

    if bestSrc == None or bestDst == None:
        return
        
    srcShips = bestSrc.num_ships() + changesList[bestSrc.planet_id()]
    dstShips = bestDst.num_ships()
    if srcShips > dstShips:
        bestNumOfShips = dstShips + 600 #add 600 because the enemy will load more sheeps
        if bestNumOfShips > srcShips:
            bestNumOfShips = srcShips
    else:
        bestNumOfShips = srcShips / 2 #send half of my ships

    updateChangesList(bestSrc, bestDst, bestNumOfShips)
    attackersList.append(bestSrc.planet_id())
    pw.issue_order(bestSrc, bestDst, bestNumOfShips)

def do_turn(pw):
    resetChangesList(pw)
    resetAttackersList()
    if len(pw.my_planets()) == 0 or len(pw.not_my_planets()) == 0:
        return
    
    defend(pw)
    for i in range(15):
            attack(pw)